#include <iostream>

using namespace std;

void tribulle(int tab[], int taille);

int main()
{
    //code test
    int tab[10] = { 5, 6, 154, 103, -7, 29, 0, 17, 45, 56};

    for (int i=0; i < 10; ++i)
    {
        cout << tab[i] << " ";
    }
    tribulle(tab,10);
    cout << endl;

    cout << "Voici le tableau triée selon la méthode du tri à bulles" << endl;
    for (int i=0; i < 10; ++i)
    {
        cout << tab[i] << " ";
    }

    return 0;
}

void tribulle(int tab[], int taille)
{

    int temp;
    for (int i = 0; i < taille; i++) //parcourir le tableau
    {
        for (int j = 0; j < taille - 1; j++) //parcourir les paires
        {
            // trouver le plus petit de la paire
            if (tab[j] > tab[j + 1])
            {
                //echange de la paire
                temp = tab[j];
                tab[j] = tab[j + 1];
                tab[j + 1] = temp;
            }
        }
    }
}
