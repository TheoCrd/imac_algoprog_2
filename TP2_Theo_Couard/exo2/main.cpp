//Tri par insertion

#include <iostream>

using namespace std;


void triinsertion(int tab[],int size);

int main()
{
    //code test
    int tab[10] = { 5, 6, 154, 103, -7, 29, 0, 17, 45, 56};

    for (int i=0; i < 10; ++i)
    {
        cout << tab[i] << " ";
    }
    triinsertion(tab,10);
    cout << endl;

    cout << "Voici le tableau triée selon la méthode du tri par insertion" << endl;
    for (int i=0; i < 10; ++i)
    {
        cout << tab[i] << " ";
    }

    return 0;
}

void triinsertion(int tab[],int taille)
{
    int j,temp;

    for (int i=1 ; i <= taille-1; i++) //on part de l'indice 1
    {
        j = i;

        while (j > 0 && tab[j-1] > tab[j]) // on parcourt le tableau et on compare l'indice courant avec le precedent
        {
            temp = tab[j];
            tab[j] = tab[j-1];
            tab[j-1] = temp;
            j--;
        }
    }
}
