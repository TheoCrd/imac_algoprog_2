#include <iostream>

using namespace std;

void fuuuuuusionha(int tab[], int a, int b, int c);
void trifusion(int tab[], int start, int end);

int main()
{
    //code test
    int tab[10] = { 5, 6, 154, 103, -7, 29, 0, 17, 45, 56};

    for (int i=0; i < 10; ++i)
    {
        cout << tab[i] << " ";
    }
    trifusion(tab,0,10-1);
    cout << endl;

    cout << "Voici le tableau triée selon la méthode du tri fusion" << endl;
    for (int i=0; i < 10; ++i)
    {
        cout << tab[i] << " ";
    }

    return 0;
}

// trier deux sous tableau t1 et t2 de tab et les fusionner dans tab

void fuuuuuusionha(int tab[], int a, int b, int c) {

// On créer deux sous tableau de tab (tab est le tableau principal)

  //taille des tableaux (size1+size2 = taille de tab)
  int size1 = b - a + 1; // a indice de la premiere valeur et b derniere valeur du premier sous tableau
  int size2 = c - b; // c indice de la derniere valeur du deuxieme sous tableau

  int t1[size1], t2[size2];

    for (int i = 0; i < size1; i++){
        t1[i] = tab[a + i]; //on remplit t1
    }

    for (int j = 0; j < size2; j++){
        t2[j] = tab[b + 1 + j]; //on remplit t2
    }

  // indice des sous tableau et du tableau
  int i, j, k;
  i = 0; //indice de t1
  j = 0; // indice de t2
  k = a; //indice de tab (pareil que premiere valeur de t1)

  //comparer les valeurs des sous tableaux puis mettre dans tab la plus petite
  while (i < size1 && j < size2) {
    if (t1[i] <= t2[j]) {
      tab[k] = t1[i];
      i++;
    }
    else {
      tab[k] = t2[j];
      j++;
    }
    k++; //on avance dans tab
  }

//Si on arrive en fin d'un tableau, on prend la valeur restante et on l'insere dans tab
  //Si on arrive en fin de t2 alors :
  while (i < size1) {
    tab[k] = t1[i]; //on prend la valeur de t1
    i++;
    k++;
  }

  //Si on arrive en fin de t1 alors :
  while (j < size2) {
    tab[k] = t2[j]; //on prend la valeur de t2
    j++;
    k++;
  }
}

//diviser pour mieux regner (on separe tab en 2 sous tableau) , on trie ensuite puis on fusionne les deux sous tableau
void trifusion(int tab[], int start, int end){

    //on trouve le milieu en prenant deux valeurs, l'une au debut et l'autre a la fin du tableau
    if (start < end) {

    // on separe tab en deux en prenant middle comme milieu
    int middle = start + (end - start) / 2;

    trifusion(tab, start, middle); //premier sous tableau
    trifusion(tab, middle + 1, end); //second sous tableau

    // fusionner les 2 sous tableau
    fuuuuuusionha(tab, start, middle, end);
  }
}
