#include <iostream>

using namespace std;

void triselec(int tab[],int taille);

int main()
{
    //code test
    int tab[10] = { 5, 6, 154, 103, -7, 29, 0, 17, 45, 56};

    for (int i=0; i < 10; ++i)
    {
        cout << tab[i] << " ";
    }
    triselec(tab,10);
    cout << endl;

    cout << "Voici le tableau triée selon la méthode du tri par selection" << endl;
    for (int i=0; i < 10; ++i)
    {
        cout << tab[i] << " ";
    }

    return 0;
}
void triselec(int tab[],int taille)
{
    int mini,temp;

for (int i=0; i < (taille-1); i++) //parecourir le tableau
  {
    mini = i; //definition d'un minimuym

    for (int j = i + 1; j < taille; j++) //regarder la case suivante
    {
      if (tab[j] < tab[mini])
        mini = j; //mini prend la valeur de j
    }
      //echange pour mettre le mini à la bonne place
      temp = tab[i];
      tab[i] = tab[mini];
      tab[mini] = temp;
  }
}
