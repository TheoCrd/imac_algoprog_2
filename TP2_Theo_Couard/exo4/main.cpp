#include <iostream>

using namespace std;

void trirapide(int tab[], int size);

int main()
{
    //code test
    int tab[10] = { 5, 6, 154, 103, -7, 29, 0, 17, 45, 56};

    for (int i=0; i < 10; ++i)
    {
        cout << tab[i] << " ";
    }
    trirapide(tab,10);
    cout << endl;

    cout << "Voici le tableau triée selon la méthode du tri rapide" << endl;
    for (int i=0; i < 10; ++i)
    {
        cout << tab[i] << " ";
    }

    return 0;
}

//diviser pour mieux regner (un tableau inferieur au pivot, un tableau superieur au pivot)
void trirapide(int tab[], int size){
    int pivot;
    int inf = 0; //indice du tableau < pivot
    int sup = 0; //indice du tableau > pivot

    pivot = tab[0]; //on prend le premier élément de tab comme pivot
    int infsize=0; //taille du tableau inferieur au pivot

    //on definit la valeur de infsize
    for(int i=0; i<size; i++){
        if(tab[i]<pivot){
            infsize++;
        }
    }

    int supsize = 0; //taille du tableau superieur au pivot

    //on definit la valeur de supsize
    for(int i=0; i<size; i++){
        if(tab[i]>pivot){
            supsize++;
        }
    }
     int infpivot[infsize]; //creation du sous tableau avec les valeurs < pivot
     int suppivot[supsize]; //creation du sous tableau avec les valeurs > pivot

     if(size == 0){ //si le tableau est vide
        return;
    }

    for (int i=1; i<size; i++){

        if (tab[i]<pivot){
            infpivot[inf]=tab[i];
            inf++;
        }
        else if (tab[i]>pivot){
            suppivot[sup]=tab[i];
            sup++;
        }
    }
        trirapide(infpivot,infsize); //trier le tableau avec les éléments inferieurs au pivot
        trirapide(suppivot,supsize); //trier le tableau avec les éléments supérieurs au pivot


 	// on fusionne les tableaux

	for (int j=0; j<infsize; j++){
		tab[j]=infpivot[j];

	}

	tab[infsize]= pivot; //on place le pivot dans tab

	for (int j=infsize+1; j<size; j++){
		tab[j] = suppivot[j-infsize-1];
	}
}
