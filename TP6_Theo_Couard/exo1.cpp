#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
	/**
	  * Make a graph from a matrix
	  * first create all nodes, add it to the graph then connect them
	  * this->appendNewNode
	  * this->nodes[i]->appendNewEdge
	  */
    for (int i = 0; i<nodeCount; i++)
    {
        GraphNode* node = new GraphNode(i);
        this->appendNewNode(node);
    }

    for (int j = 0; j<nodeCount; j++)
    {
        for (int k = 0; k<nodeCount; k++)
        {
            if(adjacencies[j][k] != 0)
            {
                this->nodes[j]->appendNewEdge(nodes[k],adjacencies[j][k]);
            }
        }
    }
}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	  * Fill nodes array by travelling graph starting from first and using recursivity
	  */


    Edge* newedge = first->edges;
    while (newedge != nullptr)
    {
        if(!visited[newedge->destination->value])
        {
            nodes[nodesSize] = newedge->destination;
            nodesSize++;
            visited[first->value] = true;
            deepTravel(newedge->destination,nodes,nodesSize,visited);
        }
        newedge = newedge->next;
    }
}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	 * Fill nodes array by travelling graph starting from first and using queue
	 * nodeQueue.push(a_node)
	 * nodeQueue.front() -> first node of the queue
	 * nodeQueue.pop() -> remove first node of the queue
	 * nodeQueue.size() -> size of the queue
	 */
	std::queue<GraphNode*> nodeQueue;

	nodeQueue.push(first);
    while (!nodeQueue.empty())
    {
        GraphNode* newgraph = nodeQueue.front();
        nodeQueue.pop();
        nodes[nodesSize] = newgraph;
        nodesSize++;
        visited[first->value] = true;
        Edge* newedge = newgraph->edges;

        while (newedge != nullptr)
        {
            if(!visited[newedge->destination->value])
            {
                nodeQueue.push(newedge->destination);
                visited[newedge->destination->value] = true;
            }
            newedge = newedge->next;
        }
    }
}

bool Graph::detectCycle(GraphNode *first, bool visited[])
{
	/**
	  Detect if there is cycle when starting from first
	  (the first may not be in the cycle)
	  Think about what's happen when you get an already visited node
	**/
    visited[first->value] = true;
    Edge* newedge = first->edges;

    while (newedge != nullptr)
    {
        if(visited[newedge->destination->value])
        {
            return true;
        }
        else
        {
            detectCycle(newedge->destination,visited);
        }
    }
    return false;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}
