#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    int leftIndex;
    leftIndex=(nodeIndex*2)+1;
    return leftIndex;
}

int Heap::rightChild(int nodeIndex)
{
    int rightIndex;
    rightIndex=(nodeIndex*2)+2;
    return rightIndex;
}

void Heap::insertHeapNode(int heapSize, int value)
{
    // use (*this)[i] or this->get(i) to get a value at index i
    int i = heapSize;
    this->get(i)=value;
    while(i>0 && this->get(i)>this->get((i-1)/2))
    {
        swap(i,(i-1)/2);
        i=(i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
    // use (*this)[i] or this->get(i) to get a value at index i
    int indexmax = nodeIndex;
    int leftIndex; 
    int rightIndex; 
    leftIndex = this->leftChild(nodeIndex);
    rightIndex = this->rightChild(nodeIndex);

    if(leftIndex < heapSize && this->get(indexmax) < this->get(leftIndex))
    {
        indexmax=this->leftChild(nodeIndex);
    }

    if(rightIndex < heapSize && this->get(indexmax)<this->get(rightIndex))
    {
        indexmax=this->rightChild(nodeIndex);
    }

    if(indexmax!=nodeIndex)
    {
        swap(nodeIndex,indexmax);
        this->heapify(heapSize,indexmax);
    }
}

void Heap::buildHeap(Array& numbers)
{
    for (uint i=0; i<numbers.size(); i++)
    {
        heapify(i,i);
    }
}

void Heap::heapSort()
{
    for (int i = this->size()-1; i>0; i--)
    {
        this->swap(0,i);
        this->heapify(i, 0);
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
    w->show();

    return a.exec();
}
