#include <iostream>

using namespace std;

//Prototype
int binarySearch(int tab[], int start, int end, int toSearch);

int tab[10] = {0,1,2,3,4,5,10,18,19,20}; //tableau trié
int toSearch;
int start = 0;
int endtab = 10;

int main()
{
    cout << "Saisir une valeur à chercher:" << endl;
    cin >> toSearch;
    cout << "La valeur cherché est à l'index " << binarySearch(tab,start,endtab,toSearch) << " du tableau." << endl;
}

int binarySearch(int tab[], int start, int end, int toSearch){

    while(start < end){
     int mid = (start + end) / 2;

        if (toSearch > tab[mid]){
            start = mid + 1;
        }
        else if (toSearch < tab[mid]){
            start = mid;
        }
        else{ //si l'index correspond à la valeur cherché
            return mid;
        }
    }
    return -1;
    cout << "La valeur " << toSearch << " n'est pas dans le tableau." << end;
}
