#include <iostream>

using namespace std;

//Prototype
int search(int value,int array[], int size);

int main()
{
    int array[5] = {1,2,38,52,19};
    int value; //une valeur entiere
    cout << "Saisissez une valeur entiere" << endl;
    cin >> value;
    cout << "Votre valeur est à l'indice " << search(value,array,5) << " du tableau" << endl;

    return 0;
}

int search(int value,int array[], int size)
{
    if (size<0)
    {
        return -1;
    }
    else if (array[size]==value)
    {
        return size;
    }
    else
    {
        return search(value, array, size-1);
    }
}
