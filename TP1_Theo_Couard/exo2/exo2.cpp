#include <iostream>

using namespace std;

//Prototype
int fibonacci(int n);

int main()
{
    int n; //une valeur entiere
    cout << "Saisissez une valeur pour n" << endl;
    cin >> n;
    cout << "La " << n << "-ième valeur de fibonacci est " << fibonacci(n) << endl;
    return 0;
}


int fibonacci(int n)
{
    if (n == 0)
    {
        return 0;
    }
    else if (n==1)
    {
        return 1;
    }
    else
    {
        return fibonacci((n-1)) + fibonacci((n-2));
    }
}
