#include <iostream>

using namespace std;

//Prototype
int power(int value,int n);

int main()
{
    int value,n; //une valeur entiere
    cout << "Saisissez une valeur" << endl;
    cin >> value;
    cout << "Saisissez une puissance n" << endl;
    cin >> n;
    cout << value <<" à la puissance " << n << " est " << power(value,n) << endl;
    return 0;
}


int power(int value, int n){
      int result;

        if (n == 0){
           result = 1;
        }
        else{
               result = value * power(value,n-1);
            }
        return result;
}
