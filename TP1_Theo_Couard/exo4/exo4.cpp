#include <iostream>

using namespace std;

//Prototype
void allEvens(int evens[], int arra[], int evenSize, int arraySize);

int main()
{
    int eventab[10]={0,0,0,0,0,0,0,0,0,0};
    int arra[10]={1,2,3,4,5,6,7,8,9,10};
    allEvens(eventab,arra,0,10);
    for (int i=0; i<10; i++)
    {
        cout << eventab[i];
    }
    return 0;
}

void allEvens(int evens[], int arra[], int evenSize, int arraySize)
{
    if (arraySize>=0)
    {

        if ((arra[arraySize]%2)==0) //si on est paire
        {
        //cout << evenSize <<endl;
            evens[evenSize] = arra[arraySize]; //on rempli evens avec les valeurs paire de arra
            evenSize++;
        }
    allEvens(evens,arra,evenSize,--arraySize); //recurisivite
    }
}
