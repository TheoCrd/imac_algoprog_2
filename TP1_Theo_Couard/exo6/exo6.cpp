#include <iostream>
#include <stdlib.h>

using namespace std;

struct Noeud{
    int data;
    Noeud* next; //valeur suivante
};

struct Liste{
    Noeud* first; //premiere valeur
    Noeud* last;  //derniere valeur
};

struct DynaTableau{
    int* data; //tableau des donnees
    int capacity; //taille du tablea
    int nbvalue; //nombre de valeurs
};


void initialiseListe(Liste* liste)
{
    //Initialiser les premiers et derniere valeurs
    liste->first = NULL;
	liste->last = NULL;
}

// Attention traitement de la structure du tableau ici
void initialiseDyna(DynaTableau* tableau, int capacity)
{
    tableau->data = (int*) malloc(sizeof(int)*capacity); //allocation de memoire
    tableau->capacity = capacity;
    tableau->nbvalue = 0;
}

bool est_vide(const Liste* liste)
{
    if (liste->first == NULL){ // on verifie si il y a un premier element
		return true;
	}
    return false;
}

void ajoute(Liste* liste, int value)
{
    if (liste->last != NULL){
		liste->last = liste->last->next; //on passe � l'element suivant
	} else {
		liste->last = liste->first; //le dernier vient se mettre en debut de liste
	}
	liste->last->data = value; // ajoute une valeur en fin de liste
	liste->last->next = NULL; // initialise le suivant
}

void affiche(const Liste* liste)
{
    Noeud* top = liste->first; // top est le premier element (en tete de liste)
    do
    {
        cout << top->data << endl; // on affiche la valeur de l'el�meent
		top = top->next; // on passe � l'�l�ment suivant de la liste
    }while (top!=NULL); //on affiche tant que l'on ne rencontre rien de NULL
}

int recupere(const Liste* liste, int n)
{
    Noeud* position_int = liste->first;
	int i=0;
	while (position_int != NULL && i<n){
		position_int = position_int->next; //on passe au suivant tant que l'on n'est pas arriv� � n ou cas ou c'est NULL
		i++;
	}
		return position_int->data; //on retourne l'entier specifi� dans la liste

}

int cherche(const Liste* liste, int value)
{
    Noeud* top = liste->first; //top est le premier de la liste
	int i=0;
	do
    {
       if (top->data == value){ // si la donnee de la liste est la valeur
			return i; // on retourne son indice
		top = top->next;
		i++;
       }
    }while (top!=NULL); //tant que top n'est pas NULL on parcourt la liste
    return -1; // n'existe pas
}

void stocke(Liste* liste, int n, int value)
{
    Noeud* top = liste->first;
    int i = 0;
	while (i<n){
		top = top->next; //on continue de passer azu suvant tant qu'on n'a pas atteint n
        i++;
	}
	top->data = value; //on remplace la valeur

}

//On revient a la structure du tableau
void ajoute(DynaTableau* tableau, int value)
{
	tableau->data[tableau->nbvalue]=value;
	tableau->nbvalue+=1;
}


bool est_vide(const DynaTableau* liste)
{
    return liste->nbvalue==0; //si on est egale � 0 (vide dans la structure) retourne false
}

void affiche(const DynaTableau* tableau)
{
    for (int i = 0; i < tableau->nbvalue; i++){
		cout << tableau->data[i] << endl; //affiche la donnee du tableau
	}
}

int recupere(const DynaTableau* tableau, int n)
{
    if (n < tableau->nbvalue && tableau->nbvalue > 0){ //si n est superieur � 0 et aux nombres de valeurs la structure
		return tableau->data[n]; //retourne le nieme entier de la structure
	}
    return 0;
}

int cherche(const DynaTableau* tableau, int value)
{
    for (int i = 0; i < tableau->nbvalue; i++){
		if (tableau->data[i] == value){
			return i; //retourne l'index de valuer
		}
	}
    return -1;
}

void stocke(DynaTableau* tableau, int n, int value)
{
		tableau->data[n] = value; //la nieme valeur est remplac�e par value
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int value)
{
    ajoute(liste, value); //ajouter en fin de structure
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    if (liste->first != NULL){ //verifification liste non vide
		Noeud* top = liste->first; //top est premier de la liste
		liste->first = liste->first->next; //premier devient suivant
        int value = top->data; //valeur � ajouter
		free(top); //enlever la premiere valeur ajoutee
		return value; //retouner la premiere valeur ajoutee
	}
    return 0;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int value)
{
    ajoute(liste,value);
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{   //presque comme pour la file
    if (liste->first != NULL){
		Noeud* top = liste->first;
		liste->first = liste->first->next;
        int value = liste->first->data;
		free(top);
		return value;
	}
    return 0;
}


int main()
{
    Liste liste;
    initialiseListe(&liste);
    DynaTableau tableau;
    initialiseDyna(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste � " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste � " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements apr�s stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialiseListe(&pile);
    initialiseListe(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&liste) && compteur > 0)
    {
        std::cout << retire_file(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis l�..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl; //changement de "pile" par "file"
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis l�..." << std::endl;
    }

    return 0;
}//ca n'affiche rien D:
