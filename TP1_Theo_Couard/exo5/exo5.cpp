#include <iostream>
#include <math.h>

using namespace std;

//structure vecteur (complexe)
struct vec2{
    float x;
    float y;
};

//Prototypes
bool isMandelbrot(vec2 z, vec2 point, int n);

int main()
{
    vec2 z; //l'utilisateur cr�e un complexe z
    cout << "ESPACE DE MANDELBROT" << endl;
    cout << endl;
    cout << "Cr�ation d'un complexe:" << endl;
    cout << "Saisissez la partie r�elle : " << endl;
    cin >> z.x;
    cout << "Saisissez la partie imaginaire : " << endl;
    cin >> z.y;
    cout << endl;

    vec2 point; //on creer un point
    cout << "Cr�ation d'un point:" << endl;
    cout << "Saisissez la coordonnee en abscisse : " << endl;
    cin >> point.x;
    cout << "Saisissez la coordonne en ordonnee : " << endl;
    cin >> point.y;

    int n;
    cout << "Saisissez un entier n:" << endl;
    cin >> n;
    cout << endl;

    bool test;
    test = isMandelbrot(z, point, n); //pour v�rifier

    if (test) {
        cout << "Le point X(" << point.x << "," << point.y <<") appartient � l'espace de Mandelbrot pour f(z) --> z� + point" << endl;
    }

    if (!test) {
        cout << "Le point X(" << point.x << "," << point.y <<") n'appartient pas � l'espace de Mandelbrot pour f(z) --> z� + point" << endl;
    }
 return 0;
}

bool isMandelbrot(vec2 z, vec2 point, int n){ //on cherche a savoir si un point appartient a l'espace de mandelbrot avec une fonction f(z) --> z� + point

    //on "cr�e" z au carree, ici chaque coordonnes avec z� =(x+iy)�

    vec2 z_square; // z au carree
    z_square.x = (pow(z.x,2)-pow(z.y,2)); //partie reelle
    z_square.y = (2 *(z.x)*(z.y)); //partie imaginaire

    //la fonction f(z) --> z� + point

    vec2 function_z; //fonction f(z)
    function_z.x = z_square.x + point.x;
    function_z.y = z_square.y + point.y;

    //Module de z
    float module;
    module = sqrt(pow(z.x,2)+pow(z.y,2)); //calcul du module de z

    //partie recursivite

    if (n == 0){
        return false;
    }
    return(isMandelbrot(function_z,point,n-1) || module < 2); //erreur le corrige indiquait si |z| > 2
}

